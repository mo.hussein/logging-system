const http = require('http');
const app = require('./app');
require('dotenv').config();
const port = process.env.PORT || 3000;

const index = http.createServer(app);
index.listen(port);
