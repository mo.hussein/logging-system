const express = require('express');
const app = express();
const bodyParser = require('body-parser'); //to parse the incoming requests
const mongoose = require('mongoose'); //mongoose package used for mongodb
var cors = require('cors');
require('dotenv').config();

//Swagger for api docs
var swaggerOptions = require('../server/config/swagger.js');
const expressSwagger = require('express-swagger-generator')(app);
expressSwagger(swaggerOptions);

//Routes
const logRoutes = require('./routes/logs');

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/api/logs', logRoutes);

const mongoDbUri = process.env.MONGO_URI;

//connect with mongoDB
mongoose.connect(mongoDbUri);
mongoose.connection.on('connected', () => {
    console.log('Database Connected Successfully');
});
mongoose.connection.on('error', err => {
    console.log('error while connecting to mongodb', err);
});


//Header Protection (For Browsers)
app.use((req, res, next)=>{
   res.header("Access-Control-Allow-Origin", '*');
   res.header(
       "Access-Control-Allow-Headers",
       "Origin, X-Requested-With, Content-Type, Accept, Authorization"
   );
   if(req.method === 'OPTIONS'){
       res.header('Access-Control-Allow-Methods', 'POST, PATCH, GET, DELETE');
       return res.status(200).json({});
   }
   return next();
});

//Start Error Handling from all requests (middleware)
app.use((req, res, next)=>{
    const error = new  Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message: error.message
        }
    });
});
//End of Error Handling


module.exports = app;
