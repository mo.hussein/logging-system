const express = require('express');
const router = express.Router();
const logController = require('../controllers/logs');
var app = express();

/**
 * @route GET /logs
 * @group logs - View logs
 * @returns {object} 200 - All logs fetched!
 * @returns {Error}  404 - Error fetching logs
 */
router.get('/', logController.find);

/**
 * @route GET /logs/{id}
 * @group logs - Get log details
 * @param {string} id.path.required - ID - eg: 5d6c2f10f50bfa37e06ab8e1
 * @returns {object} 200 - log details
 * @returns {Error} 404  - Not a valid entry found for provided ID
 */
router.get('/:id', logController.findOne);

/**
 * This function comment is parsed by doctrine
 * @route POST /logs
 * @param {string} title.body.required - title
 * @param {string} description.body.required - description
 * @param {string} path.body.required - path
 * @param {number} status_code.body.required - status code - eg: 201
 * @group logs - Create Log
 * @returns 201 - An object of log details
 * @returns  default - Unexpected error
 */

router.post('/', logController.create);
module.exports = router;
