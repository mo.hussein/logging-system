const mongoose = require('mongoose');
const Log = require('../models/logs');
const base_url = "http://localhost:3000/api/logs";


exports.find = (req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var queryCond = {};
    if(req.query.title){
        queryCond.title= new RegExp(req.query.title.toLowerCase(), 'i');
    }
    if(req.query.description){
        queryCond.description= new RegExp(req.query.description.toLowerCase(), 'i');
    }
    if(req.query.status_code){
        queryCond.status_code = req.query.status_code;
    }
    var pageOptions = {
        page: req.query.page || 0,
        limit: parseInt(req.query.limit) || null
    }
    Log.find().or(queryCond).select("-__v")
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit).exec().then(docs => {
        if(docs.length >= 0)
        {

            const response = {
                count: docs.length,
                logs: docs.map(doc => {
                    return{
                        id: doc._id,
                        title: doc.title,
                        description: doc.description,
                        status_code: doc.status_code,
                        path:doc.path,
                        createdAt:doc.createdAt,
                        request: {
                            type: 'GET',
                            url: base_url+'/'+doc._id
                        }
                    }}),
                message: "All logs fetched!"
            }
            res.status(200).json(response);
        }else{
            res.status(200).json({
                message:'no data'
            })
        }

    }).catch(error => {
        res.status(404).json({
            message:'Error fetching logs',
            error:error
        })
    })
};

exports.create = (req, res, next)=>{

                const log = new Log({
                    _id: new mongoose.Types.ObjectId(),
                    title: req.body.title,
                    description: req.body.description,
                    status_code: req.body.status_code,
                    path: req.body.path,
                });

                log.save()
                    .then(result =>{
                        res.status(201).json({
                            message:"Log Created!",
                            log: {
                                _id: result._id,
                                title: result.title,
                                request:{
                                    type: 'GET',
                                    url: 'http://localhost:3000/api/logs/'+result._id
                                }
                            }
                        });

                    })
                    .catch(error => {
                        res.status(500).json({
                            error: error
                        });
                    });

};

exports.findOne =  (req, res, next)=>{
    const id = req.params.id;
    Log.findById(id).select("-__v").exec().then(doc => {
        if(doc){
            res.status(200).json({
                log: doc,
                message:"Log Fetched Successfully"
            });
        }else{
            res.status(404).json({
                message:"Not a valid entry found for provided ID"
            });
        }

    }).catch(error => {
        res.status(500).json({
            error:error
        });
    });
};

