/**
* @typedef Log
* @property {ObjectId} id
* @property {string} title.required
* @property {string} description.required
* @property {integer} status_code.required
* @property {string} path.required
*/

const mongoose = require('mongoose');
const logSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    description: {type: String, required: true},
    status_code: {type: Number, required: true},
    path: {type: String, required: false},



}, {timestamps: { createdAt: true, updatedAt: false }});


module.exports = mongoose.model('Log', logSchema);

//title, description, status code, log path and created at date.