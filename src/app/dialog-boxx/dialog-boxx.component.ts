import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface LogData {
  title: string;
  description: string;
  path: string;
  status_code: string;
}


@Component({
  selector: 'app-dialog-boxx',
  templateUrl: './dialog-boxx.component.html',
  styleUrls: ['./dialog-boxx.component.css']
})
export class DialogBoxxComponent {

  action: string;
  local_data: any;
  log_data: any;

  constructor(
    public dialogRef: MatDialogRef<DialogBoxxComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: LogData) {
    this.local_data = {...data};
    this.action = this.local_data.action;
    this.log_data = this.local_data.data;
  }

  doAction() {
    this.dialogRef.close({event: this.action, data: this.local_data});
  }

  closeDialog() {
    this.dialogRef.close({event: 'Cancel'});
  }

}
