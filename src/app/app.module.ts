import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogsTableComponent } from './logs-table/logs-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {HttpClientModule} from '@angular/common/http';
import { LogService } from './services/log.service';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DialogBoxxComponent } from './dialog-boxx/dialog-boxx.component';
@NgModule({
  declarations: [
    AppComponent,
    LogsTableComponent,
    DialogBoxxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    HttpClientModule
  ],
  entryComponents: [
    DialogBoxxComponent
  ],
  providers: [LogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
