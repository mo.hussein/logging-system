import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of as observableOf, merge } from 'rxjs';
import {Log} from '../models/log.model';
@Injectable({
  providedIn: 'root'
})
export class LogService {
  private serviceUrl = 'http://localhost:3000/api/logs';
  constructor(private http: HttpClient) { }
  getLogs(): Observable<any> {
    return this.http.get(this.serviceUrl);
  }
  getLogsFilter(obj): Observable<any> {
    return this.http.get(this.serviceUrl, {
      params: obj
    });
  }
  getLog(id): Observable<any> {
    return this.http.get(this.serviceUrl + '/' + id);
  }

  createLog(log) {
    return this.http.post(this.serviceUrl,
      log);
  }
}
