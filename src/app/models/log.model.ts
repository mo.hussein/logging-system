export interface Log {
  id: string;
  title: string;
  description: string;
  status_code: string;
  path: string;
  createdAt: string;
}
