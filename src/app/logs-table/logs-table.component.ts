import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { LogsTableDataSource, LogsTableItem } from './logs-table-datasource';
import { LogService } from '../services/log.service';
import { DialogBoxxComponent } from '../dialog-boxx/dialog-boxx.component';
import {MatDialog} from '@angular/material';
import {Observable} from 'rxjs';
@Component({
  selector: 'app-logs-table',
  templateUrl: './logs-table.component.html',
  styleUrls: ['./logs-table.component.css']
})
export class LogsTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<LogsTableItem>;
  dataSource: LogsTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'title', 'status_code' , 'createdAt', 'action'];
  constructor(public dialog: MatDialog, public rest: LogService) {}
  openDialog(action, obj) {
    obj.action = action;
    if (action === 'View') {
      this.rest.getLog(obj.data).subscribe((data: {}) => {
        obj.data =  data['log'];
        this.dialog.open(DialogBoxxComponent, {
          width: '350px',
          data: obj
        });
      });
    }
    else if (action === 'Add') {
      const dialogRef = this.dialog.open(DialogBoxxComponent, {
        width: '250px',
        data: obj
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result.event === 'Add') {
          this.addLog(result.data);
        }
      });
    }
  }
  ngOnInit() {
    this.dataSource = new LogsTableDataSource();
    this.getLogs();
  }
  getLogs() {
    this.rest.getLogs().subscribe((data: {}) => {
     this.dataSource.data =  data['logs'] as LogsTableItem[];
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  applyFilter(obj) {
     if (obj.value.length < 3 && obj.value.length !== 0) {
        return false;
     }
     console.log(this.dataSource.data);
     const params = {};
     const name = obj.name;
     const value = obj.value;
     params[name] = value;
     console.log(obj.name);
     this.rest.getLogsFilter(params).subscribe((data: {}) => {
       this.dataSource = new LogsTableDataSource();
       this.dataSource.sort = this.sort;
       this.dataSource.paginator = this.paginator;
       this.table.dataSource = this.dataSource;
       this.dataSource.data =  data['logs'];
     });
  }

  addLog(logData) {
    this.rest.createLog(logData).subscribe((data: {}) => {
      this.dataSource = new LogsTableDataSource();
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.getLogs();
    });
  }
}
