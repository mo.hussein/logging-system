# Logging System

Log System help to detects issues, bugs or craches.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

For starting server side Run `node index` inside server folder.


## Overview
Front-end using Angular 8 and angular material datatable. Back-end using node.js and express.js for API.

## API Refrence
Swagger API documentation will be found at `http://localhost:3000/api-docs`


